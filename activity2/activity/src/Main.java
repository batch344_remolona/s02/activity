import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {

        int[] primeArray = new int[5];

        primeArray[0] = 2;
        primeArray[1] = 3;
        primeArray[2] = 5;
        primeArray[3] = 7;
        primeArray[4] = 11;

        System.out.println("\t The first prime num is: " + primeArray[0]);

        ArrayList<String> friends = new ArrayList<>(Arrays.asList("John", "Bj", "Coleen", "Fifi"));

        System.out.println("\n\t My friends are: " + friends);


        HashMap<String, Integer> inventories = new HashMap<>(){
            {
                put("ToiletPaper", 11);
                put("Shampoo", 5);
                put("Cotton buds", 35);
            }
        };

        System.out.println("\n\t Our current inventory consists of: " + inventories);
    }
}